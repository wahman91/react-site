import axios from 'axios'
var contentful = require('contentful');

export default {
  getSiteProps: () => ({
    title: 'React Static',
  }),
  getRoutes: async () => {
    var client = contentful.createClient({
      space: 'ra133tykmfrg',
      accessToken: '308bee9558df8ec0f9a957b530ddb8b2506c128581d9acc81dc2007479675b0e'
    });
    var entries = await client.getEntries({'content_type': 'game'});
    var games = [];
    entries.items.forEach(function (entry) {
      var game = entry.fields;
      entry.fields.image;
      game.image = 'https:' + game.image.fields.file.url;
      games.push(game);
    });
    return [
      {
        path: '/',
        component: 'src/containers/Home',
        getProps: () => ({
          games,
        }),
        children: games.map(game => ({
          path: `/game/${game.path}`,
          component: 'src/containers/Game',
          getProps: () => ({
            game,
          }),
        })),
      },
      {
        path: '/about',
        component: 'src/containers/About',
      },
      {
        is404: true,
        component: 'src/containers/404',
      },
    ]
  },
}
