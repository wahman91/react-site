import React from 'react'
import { Router, Link, Head } from 'react-static'
import { Transition } from 'react-transition-group'
import Routes from 'react-static-routes'
require('typeface-open-sans');

import './app.css'

const duration = 300;





const Fade = ({ in: inProp }) => (
<Transition in={inProp} timeout={duration} appear={true}>
  {(state) => (
  <div className="content" >
    <Routes />
  </div>
  )}
</Transition>
);

class App extends React.Component {
  constructor (...args) {
    super(...args);
    this.state = {show: true}

  }

  render () {
    return (
    <Router>
      <div>
        <Head><title>Wahman</title></Head>
        <nav>
          <Link to="/">Spel</Link>
          <Link to="/about">Om</Link>
        </nav>
        <Fade in={this.state.show} />
      </div>
    </Router>
    )
  }
}

export default App
