import React from 'react'
import { getRouteProps, Link, Head } from 'react-static'

//

export default getRouteProps(({ game }) => (
<div>
  <Head><title>{game.game}</title></Head>

  <div class="container">
    <Link to="/">{'<'} Back</Link>
    <br />
    <h3 className="game-title">{game.game}</h3>
    <img className="game-image" src={game.image} />
    <p className="game-description pre-line" dangerouslySetInnerHTML={{ __html: game.description }} />
    <a target="_blank" href={game.play_link}>Klicka här för att se ett klipp från spelet på Youtube</a>
  </div>
</div>
))
