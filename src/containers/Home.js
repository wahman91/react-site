import React, {Component} from 'react'
import { getRouteProps, Link } from 'react-static'


class Home extends Component{

  state = {filter : ''};
  onChange = (e) => {
    this.setState({filter: e.target.value})
  };

  render(){
    const {games} = this.props;
    const {filter} = this.state;
    const filteredGames = games.filter((game) => {
      return game.game.toLowerCase().includes(filter.toLowerCase());
    });

    return (
    <div>
      <h1>Spel</h1>
      <input type="text" value={filter} placeholder="Sök" onChange={this.onChange}/>
      <div className="game-container content-center ">
        {filteredGames.map(game => (
        <li className="style-none m-8" key={game.game}>
          <Link to={`/game/${game.path}/`}>
            <div className="card">
              <div className="card-image-container">
                <img className="card-image" src={game.image} />
              </div>
              <div className="text-container">
                {game.game}
              </div>
            </div>
          </Link>
        </li>
        ))}
      </div>
    </div>
    )
  }
}
export default getRouteProps(Home)
